from flask import Flask, render_template, request, jsonify
import requests
import io
from PIL import Image

app = Flask(__name__)

API_URL = "https://xdwvg9no7pefghrn.us-east-1.aws.endpoints.huggingface.cloud"
HEADERS = {
    "Accept": "image/png",
    "Authorization": "Bearer VknySbLLTUjbxXAXCjyfaFIPwUTCeRXbFSOjwRiCxsxFyhbnGjSFalPKrpvvDAaPVzWEevPljilLVDBiTzfIbWFdxOkYJxnOPoHhkkVGzAknaOulWggusSFewzpqsNWM",
    "Content-Type": "application/json"
}

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/generate_comic', methods=['POST'])
def generate_comic():
    try:
        input_text = request.form['inputText']
        image_bytes = query({"inputs": input_text})
        image = Image.open(io.BytesIO(image_bytes))
        image.show()  # Display the image (for testing purposes)
        return jsonify({"status": "success"})
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)})

def query(payload):
    response = requests.post(API_URL, headers=HEADERS, json=payload)
    return response.content

if __name__ == '__main__':
    app.run(debug=True)

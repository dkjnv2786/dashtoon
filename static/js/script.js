$(document).ready(function () {
    $("#generateComic").click(function () {
        var inputText = $("#inputText").val();
        if (inputText.trim() !== "") {
            $.ajax({
                type: "POST",
                url: "/generate_comic",
                data: { inputText: inputText },
                success: function (response) {
                    if (response.status === "success") {
                        alert("Comic generated successfully!");
                    } else {
                        alert("Error generating comic: " + response.message);
                    }
                },
                error: function () {
                    alert("Error generating comic. Please try again.");
                }
            });
        } else {
            alert("Please enter text before generating the comic.");
        }
    });
});
